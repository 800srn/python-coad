#Write a program to convert given list into a single string, list may contain integers.
'''
l1=[10,20,"akash","sagar","kalumnuri",6566]
print(type(l1))
l2=(" ".join (map(str ,l1)))
print(type(l2))
print(l2)'''
'''
l3=[10,20,30,6,6,0.2,5,0,2,5]
v=(" ".join(str(v) for v in (l3)))
print(type(v))
print(v)'''


# or
def join(l1):
    l2 = (" ".join(map(str, l1)))
    print(l2)

l1=[10,20,"akash","sagar","kalumnuri",6566]
join(l1)
