
 '''4. Write a function that accepts a dictionary as an argument. If the dictionary contains duplicate values, it should return an empty dictionary.
    Otherwise, it should return a new dictionary where the values become the keys and the keys become the values.

    For example, if the dictionary contains the following key-value pairs:
    {'a': 10, 'b': 20, 'c': 20}
    the function should return an empty dictionary {} because there are duplicate values.

    On the other hand, if the dictionary contains the following key-value pairs:
    {'a': 10, 'b': 20, 'c': 30}
    the function should return a new dictionary {10: 'a', 20: 'b', 30: 'c'} where the values from the original dictionary become the keys,
    and the keys from the original dictionary become the values
'''


def mydirectry(dir):
    __name__=__main__
