'''3. Python program to remove spaces from string without any inbuilt function
    Given: "Learning Python is a fun"
    Output: LearningPythonisafun'''
a="learning python is a fun"
b="abcdefghijklmnopqrstuvwxyz"
c=""
for i in a:
    if i in b:
        c+=i

print(c)

