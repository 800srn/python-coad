# STRINF
# Indextng
'''a="Shrikant"
print(a[4])
print(a[1:6])
print(a[1:6:2])# skip 1 position
print(a[::]) # print all
print(a[-1::-1]) #reverse string'''
import dis
import re

#concatination operater (+)
'''a= "Shrikant"
b= "Nilkanthe"
print(a+" "+b)'''

# repiter operator
'''a="hallo"
print(a*5)
print(a*5,end=" ") # it dose not work'''


#in operater
'''x= " i am working "
print("m"in x)
print("f"in x)'''
 #not in
''''y= " shrikant "

print("s"not in y)
print("f" not in y)'''

# slice
'''s= "i am learning python"
print(s[4:11])
print(s[-1:3:-1])'''
# length & count
'''f="shvdpohophsozhovhohnn;zvhhgjnohsfnszlio"
print(len(f))
print(f.count("z",)) # it count the total no of given word in string'''
''
# capitalize (it only capatalize 1st alphabate of statment in to upper case
'''i="playing"
print(i.capitalize())'''


# find
'''x='rose is red'
print(x.find("is"))
print(x.find('r',1))
'''
# lower,upper & title
'''s=( "multiline statement")
print(s.upper())
f="SINGLE LINE STATEMENT"
print(f.lower())
print(f.title()) ''' # it converts  1st alphabet of each word into upper case

# swipe (it changes case of each alphabet of statement)
'''d= "swiPe THe GivEn STaTement"
print(d.swapcase())'''


# is lower, is upper ,is space
'''g= "white SpAce like Tabe spAce is given "
print(g.isspace())
print(g.islower())
print(g.isalnum())
print(g.isspace())
print(g.istitle())'''



'''# splite string
d='no special characters_are_used_in_identifiers'
print(d.split(' '))
print(d.split('_'))'''
# replace
'''a="rose is red"
print(a.replace("red","yellow"))'''

#characters removal

'''print(re.split(r'[a-j]','my name is shrikant'))'''

# find
import re      # import re is not mandatory
# print(re.findall((r'\D'),'srn2557'))
#d finds degit in string  & D for alphabates in string

#print(re.split(r'\[c]*at',("shrikant ramesh Nilkanthe")))
# result only provide [] braket

# match
'''import re      # import re is not mandatory
s="the computer screen is vary big"
match=re.search(r'screen',s)
print("starting index ,",match.start())
print("ending index,",match.end())'''

# match pattern
import re
s = "Dipti-p@yahoo.com"
match=re.search(r'[\W.-]+@[\w.-]+',s)
if match :
    print(match.group())
else:
    print("not match")